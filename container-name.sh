#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
DIR="$(basename $DIR)"
CONTAINER_NAME="$DIR"
EXTENSION="-master"
export ARCH="x86-64"
IMAGE_NAME="app-container-image-tensorflow-examples-container-${ARCH}"
REMOTE_IMAGE="tensorflow-examples"
BRANCH="x86-64"
