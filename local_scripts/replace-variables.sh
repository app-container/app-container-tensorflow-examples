#!/bin/bash
source ../container-name.sh
echo "+ find ./ -type f | xargs perl -pi -w -e 's/\<ARCH\>/${ARCH}/g;'"
find ./ -type f | xargs perl -pi -w -e 's/\<ARCH\>/$ENV{'ARCH'}/g;'
