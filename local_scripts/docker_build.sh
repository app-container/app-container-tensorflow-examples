#!/bin/bash
HERE=$(pwd)
source ../container-name.sh
set -x
rm -f ../dockerfile/${IMAGE_NAME}.tar.bz2
cp /workdir/build/container-${ARCH}-tensorflow${EXTENSION}/tmp/deploy/images/container-${ARCH}/${IMAGE_NAME}.tar.bz2 ../dockerfile/${IMAGE_NAME}.tar.bz2
# --> replace ARCH
pushd ../dockerfile
cp Dockerfile /tmp/Dockerfile.ori
${HERE}/replace-variables.sh
popd
# <-- replace ARCH
docker build --rm=true -t reslocal/${CONTAINER_NAME} ../dockerfile/
rm -f ../dockerfile/${IMAGE_NAME}.tar.bz2
pushd ../dockerfile
#git checkout Dockerfile
mv /tmp/Dockerfile.ori Dockerfile
popd
set +x
