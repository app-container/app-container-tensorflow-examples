source ../container-name.sh
IMAGE_NAME=$1

APP_TO_RUN_1="/usr/bin/tf2-qs-for-beg.py"
APP_TO_RUN_2="/usr/bin/tf2-qs-for-exp.py"

if [ $# -lt 1 ];
then
    echo "+ $0: Too few arguments!"
    echo "+ use something like:"
    echo "+ $0 <docker image>" 
    echo "+ $0 reslocal/${CONTAINER_NAME} ${APP_TO_RUN_1}"
    echo "+ $0 reslocal/${CONTAINER_NAME} ${APP_TO_RUN_2}"
    exit
fi

# remove currently running containers
echo "+ ID_TO_KILL=\$(docker ps -a -q  --filter ancestor=$1)"
ID_TO_KILL=$(docker ps -a -q  --filter ancestor=$1)

echo "+ docker ps -a"
docker ps -a
echo "+ docker stop ${ID_TO_KILL}"
docker stop ${ID_TO_KILL}
echo "+ docker rm -f ${ID_TO_KILL}"
docker rm -f ${ID_TO_KILL}
echo "+ docker ps -a"
docker ps -a

set -x
docker run --entrypoint=/bin/sh ${IMAGE_NAME} -c "${2}"
set +x

