#!/bin/bash
source ../container-name.sh

TIMESTAMP=$(date +%Y-%m-%d_%H-%M-%S)

if [ "$#" -ne 2 ]; then
    echo "Usage:"
    echo "./special_tag_push.sh <local tag>  <remote tag>"
    echo "./special_tag_push.sh latest       latest-${ARCH}-${TIMESTAMP}"
    exit
fi

set -x
docker images
docker tag reslocal/${CONTAINER_NAME}:$1 reliableembeddedsystems/${REMOTE_IMAGE}:latest-${ARCH}
docker tag reslocal/${CONTAINER_NAME}:$1 reliableembeddedsystems/${REMOTE_IMAGE}:$2
docker images
docker login --username reliableembeddedsystems
docker push reliableembeddedsystems/${REMOTE_IMAGE}:latest-${ARCH}
docker push reliableembeddedsystems/${REMOTE_IMAGE}:$2
set +x
